(setq visible-bell t
      font-lock-maximum-decoration t
      color-theme-is-global t
      truncate-partial-width-windows nil)

;;(load-theme 'leuven t)
;;(load-theme 'oldlace)
;;(load-theme 'ample-zen t)
(load-theme 'wombat)

;; Highlight current line
;;(global-hl-line-mode 1)

;; visually wrap lines
;;(global-visual-line-mode)

;; Show line numbers in buffers.
(global-linum-mode t)
(setq linum-format (if (not window-system) "%4d " "%4d"))

;; Highlight the line number of the current line.
(hlinum-activate)
(setq linum-disabled-modes
      '(term-mode slime-repl-mode magit-status-mode help-mode nrepl-mode
                  mu4e-main-mode mu4e-headers-mode mu4e-view-mode
                  mu4e-compose-mode))
(defun linum-on ()
  (unless (or (minibufferp) (member major-mode linum-disabled-modes))
    (linum-mode 1)))



(provide 'appearance)
