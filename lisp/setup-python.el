;; (add-to-list 'load-path (concat (getenv "HOME") "/coolstuff/emacs-ipython-notebook/lisp"))
;; (require 'ein)

(add-hook 'python-mode-hook 'anaconda-mode)
(add-hook 'python-mode-hook 'turn-on-eldoc-mode)

(provide 'setup-python)
