(add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)

(add-to-list 'exec-path (concat (getenv "HOME") "/bin"))

(provide 'setup-clojure)
