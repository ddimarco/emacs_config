(add-hook 'after-init-hook #'global-flycheck-mode)

;; Override default flycheck triggers
;; (setq flycheck-check-syntax-automatically '(save idle-change mode-enabled)
;;       flycheck-idle-change-delay 0.8)

;; (setq flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-list)

(add-hook 'php-mode-hook 'flycheck-mode)
(add-hook 'sh-mode-hook 'flycheck-mode)
(add-hook 'json-mode-hook 'flycheck-mode)
(add-hook 'nxml-mode-hook 'flycheck-mode)
(add-hook 'python-mode-hook 'flycheck-mode)
(add-hook 'emacs-lisp-mode-hook 'flycheck-mode)
(add-hook 'lisp-interaction-mode-hook 'flycheck-mode)

(add-hook 'js-mode-hook 'flycheck-mode)
;; (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)) ;disable the annoying doc checker
;; (setq flycheck-indication-mode 'right-fringe)

(provide 'setup-flycheck)
