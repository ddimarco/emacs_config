(add-hook 'after-init-hook 'global-company-mode)

(setq company-idle-delay 0.3)
(setq company-tooltip-limit 20)
(setq company-minimum-prefix-length 2)
(setq company-echo-delay 0)

(require 'company-anaconda)
(add-to-list 'company-backends 'company-anaconda)

(require 'company-tern)
(add-to-list 'company-backends 'company-tern)

(add-to-list 'company-backends 'company-ghc)
(custom-set-variables '(company-ghc-show-info t))

(require 'company-auctex)
(company-auctex-init)

(add-to-list 'company-backends 'company-files t)

(define-key global-map (kbd "C-,") 'company-complete)
(define-key global-map (kbd "C-.") 'company-files)

(provide 'setup-company)
