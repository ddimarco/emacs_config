(require 'helm-config)
(require 'helm)

(helm-mode 1)

;; Replace common selectors with Helm versions.
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x C-g") 'helm-do-ag)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "C-x c g") 'helm-google-suggest)
(global-set-key (kbd "C-x C-r") 'helm-recentf)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
;; mark places with C-Space C-Space
(global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
;; show all registers
(global-set-key (kbd "C-c h x") 'helm-register)

(global-set-key (kbd "C-c h o") 'helm-occur)

;; other cool stuff
;; helm-find: C-x c /
;; helm-man-woman: C-x c m
;; helm-apropos: C-x c a
;; helm-color: C-x c c
;; helm-ag: C-c p s s
;; helm-top: C-x c t
;; when in "open file mode":
;; C-c C-o: open in other window
;; C-c r: open as root

;; Enrich isearch with Helm using the `C-S-s' binding.
;; swiper-helm behaves subtly different from isearch, so let's not
;; override the default binding.
(require 'swiper-helm)
(global-set-key (kbd "C-S-s") 'swiper-helm)

;; Tell Helm to resize the selector as needed.
(helm-autoresize-mode 1)

;; Make Helm look nice.
(setq-default helm-display-header-line nil
              helm-autoresize-min-height 10
              helm-autoresize-max-height 35
              helm-split-window-in-side-p t

              helm-M-x-fuzzy-match t
              helm-buffers-fuzzy-matching t
              helm-recentf-fuzzy-match t
              helm-apropos-fuzzy-match t
              helm-semantic-fuzzy-match t)

(set-face-attribute 'helm-source-header nil :height 0.75)

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to do persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z


(provide 'setup-helm)
