(global-set-key (kbd "C-c m") 'magit-status)

(setq magit-last-seen-setup-instructions "1.4.0")

(provide 'setup-magit)
