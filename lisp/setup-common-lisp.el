(setq inferior-lisp-program "/usr/bin/sbcl")

(require 'sly-company)

(add-hook 'sly-mode-hook 'sly-company-mode)
(add-to-list 'company-backends 'sly-company)

(provide 'setup-common-lisp)
