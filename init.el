(add-to-list 'load-path (expand-file-name "better-defaults" user-emacs-directory))
(require 'better-defaults)

(setq load-prefer-newer t)

;; Set path to dependencies
(setq site-lisp-dir
      (expand-file-name "lisp" user-emacs-directory))

;; Set up load path
;;(add-to-list 'load-path user-emacs-directory)
(add-to-list 'load-path site-lisp-dir)

;; No splash screen please
(setq inhibit-startup-message t)

;; Keep emacs Custom-settings in separate file
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

;; Write backup files to own directory
(setq backup-directory-alist
      `(("." . ,(expand-file-name
                 (concat user-emacs-directory "backups")))))

;; write autosave files into /tmp
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Make backups of files, even when they're in version control
(setq vc-make-backup-files t)


;; Setup packages
(require 'setup-package)

;; Install extensions if they're missing
(defun init--install-packages ()
  (packages-install
   '(
     leuven-theme
     ;; git
     magit
     git-gutter-fringe
     ;; editing
     paredit
     hlinum
     visual-regexp
     yasnippet
     undo-tree
     highlight-symbol
     ethan-wspace
     hydra
     flycheck
     smooth-scrolling
     ;; helm
     helm
     swiper-helm
     helm-projectile
     helm-ag

     iedit
     lacarte
     ;; programming languages
     cmake-ide
     rtags
     cmake-mode
     anaconda-mode
     company
     company-anaconda
     haskell-mode
     company-ghc
     protobuf-mode
     graphviz-dot-mode
     json-mode
     js2-mode
     tern
     company-tern
     ;; (la)tex
     auctex
     company-auctex
     latex-extra
     ;; clojure
     clojure-mode
     cider
     ;; lisp
     sly
     sly-company

     ;; help
     guide-key
     discover-my-major

     highlight-escape-sequences
     elisp-slime-nav
     multiple-cursors
     smart-mode-line
     ;;ace-jump-mode
     avy
     org-bullets
     insert-shebang
     ;; experimental cpp stuff
     ;; rtags
     pdf-tools
     projectile
     quickrun
     )))

(condition-case nil
    (init--install-packages)
  (error
   (package-refresh-contents)
      (init--install-packages)))

(require 'appearance)

;; Lets start with a smattering of sanity
(require 'sane-defaults)

;; Tab indentation is a disease; a cancer of this planet.
;; Turn it off and let's never talk about this default again.
(set-default 'indent-tabs-mode nil)

;; The fact that we have to do this is also quite embarrassing.
(setq sentence-end-double-space nil)

;; Always indent after a newline.
(define-key global-map (kbd "RET") 'newline-and-indent)

;; Strict whitespace with ethan-wspace: highlight bad habits,
;; and automatically clean up your code when saving.
;; Use C-c c to instantly clean up your file.
;; Read more about ethan-wspace: https://github.com/glasserc/ethan-wspace
;;(package-require 'ethan-wspace)
(setq mode-require-final-newline nil)
(setq require-final-newline nil)
(global-ethan-wspace-mode 1)

;;(semantic-mode)

(global-set-key (kbd "C-c q") 'quickrun)

;; (require 'smartparens-config)
;; (smartparens-global-mode t)

(sml/setup)

(require 'paredit)
(add-hook 'clojure-mode-hook (lambda () (paredit-mode 1)))
(add-hook 'cider-repl-mode-hook (lambda () (paredit-mode 1)))
(add-hook 'emacs-lisp-mode-hook (lambda () (paredit-mode 1)))
(add-hook 'sly-mrepl-mode-hook (lambda () (paredit-mode 1)))
(add-hook 'sly-mode-hook (lambda () (paredit-mode 1)))

(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)

(add-hook 'scheme-mode-hook (lambda () (paredit-mode 1)))

(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))

(require 'tern)
(add-hook 'js2-mode-hook (lambda () (tern-mode t)))

;; guide-key
(require 'guide-key)
(setq-default guide-key/guide-key-sequence t
              guide-key/recursive-key-sequence-flag t
              guide-key/idle-delay 2
              guide-key/popup-window-position 'bottom
              guide-key/text-scale-amount -2)
(guide-key-mode 1)

;;(package-require 'discover-my-major)
(global-set-key (kbd "C-h C-m") 'discover-my-major)


;; Setup extensions
(require 'setup-helm)
(eval-after-load 'org '(require 'setup-org))
;; (eval-after-load 'dired '(require 'setup-dired))
(require 'setup-magit)
(require 'setup-hydra)

(require 'git-gutter-fringe)
(global-git-gutter-mode t)

(require 'setup-flycheck)
(require 'setup-yasnippet)
(eval-after-load 'haskell-mode '(require 'setup-haskell))
(require 'setup-auctex)
(require 'setup-python)
(require 'setup-common-lisp)
(require 'setup-paredit)
(require 'setup-multiple-cursors)
(require 'setup-clojure)
(require 'setup-company)
(require 'setup-projectile)

(require 'highlight-symbol)
;; edit multiple occurences with C-;
(require 'iedit)
;; automatically insert #! in scripts
(require 'insert-shebang)
(add-hook 'find-file-hook 'insert-shebang)

;; experimental cmake/cpp support
;; (let ((additional-stuff-path (expand-file-name "additional-stuff/cmake-ide" user-emacs-directory)))
;;   (when (file-directory-p additional-stuff-path)
;;     (add-to-list 'load-path additional-stuff-path)
;;     (require 'cmake-ide)
;;     (cmake-ide-setup)))
(require 'cmake-ide)
(require 'rtags) ;; optional, must have rtags installed
(cmake-ide-setup)

;;(add-to-list 'load-path (expand-file-name "additional-stuff/emacs-ipython-notebook/lisp" user-emacs-directory))
;;(require 'ein)
;;;;;;;;;;;

;; Highlight escape sequences
(require 'highlight-escape-sequences)
(hes-mode)
(put 'font-lock-regexp-grouping-backslash 'face-alias 'font-lock-builtin-face)

;; Visual regexp
(require 'visual-regexp)
(define-key global-map (kbd "M-&") 'vr/query-replace)
;;(define-key global-map (kbd "M-/") 'vr/replace)

;; (require 'ace-jump-mode)
;; (global-set-key (kbd "M-j") 'ace-jump-mode)
(require 'avy)
(global-set-key (kbd "M-j") 'avy-goto-char)

(require 'lacarte)
(global-set-key (kbd "C-c M-x") 'lacarte-execute-command)
(global-set-key (kbd "C-c M-m") 'lacarte-execute-menu-command)

;; Elisp go-to-definition with M-. and back again with M-,
(autoload 'elisp-slime-nav-mode "elisp-slime-nav")
(add-hook 'emacs-lisp-mode-hook (lambda () (elisp-slime-nav-mode t) (eldoc-mode 1)))

;; Use normal tabs in makefiles
(add-hook 'makefile-mode-hook 'indent-tabs-mode)

(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))
(add-to-list 'auto-mode-alist '("\\.m$" . octave-mode))

(add-hook 'prog-mode-hook (lambda () (interactive) (setq show-trailing-whitespace 1)))

(require 'saveplace)
(setq-default save-place t)

(require 'recentf)

;; enable recent files mode.
(recentf-mode t)

; 50 files ought to be enough.
(setq recentf-max-saved-items 50)

(setq ispell-program-name "hunspell")

(put 'downcase-region 'disabled nil)

(global-unset-key (kbd "C-z")) ;; Fuck you, `suspend-frame'
